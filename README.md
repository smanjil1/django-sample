# Sample django app, mostly targetted to simulate and try authorization and authentication.

References:

https://learndjango.com/tutorials/django-login-and-logout-tutorial


## First step: Install django and configure db
```
$ pipenv shell --python /usr/bin/python3.8
$ pipenv install
$ python manage.py migrate  (using postgres here)
$ python manage.py runserver
```

### Auth is already available in INSTALLED_APPS by default in settings.py, so no worries.

## Auth url in urls.py
```
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
]
```

-- accounts/ covers login, logout, reset and many others routes.

## Check login page in (127.0.0.1/8000/accounts/login)
![Login page](readme_images/login.png)

## Creating users (superuser)
```
$ python manage.py createsuperuser
```


