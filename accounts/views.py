
from .forms import (SignUpForm,
                    ProfileForm)
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import (CreateView,
                                  UpdateView)


class SignUpView(CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class ProfileView(UpdateView):
    model = User
    form_class = ProfileForm
    success_url = reverse_lazy('home')
    template_name = 'profile.html'
